# E-Portfolios
*Jacqueline Wüthrich*

Im M123 geht es um DHCP, DNS und SMB

- [DNS](DNS/DNS.md)
- [DHCP](<DHCP_LAB/DHCP Dienst zur Verfügung stellen.md>)
- [Samba](SMB/SAMBA_Share.md)
- [Print](Printserver/Printserver.md)
- [Filius_Netzwerk](Filius_Netzwerk/Filius_Netzwerk.md)
