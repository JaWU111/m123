[TOC]
# DHCP-Dienst bereitstellen
Im Modul 123 bestand die Aufgabe darin, einen DHCP-Server zu konfigurieren. Dafür waren ein Linux-Server und ein Windows 10-Client erforderlich.

## Erstellen von VMs
Für den Windows-Client wurde eine Kopie einer VM aus Modul 117 verwendet. Für den Ubuntu-Server wurde eine neue VM erstellt.

Die Konfiguration des Ubuntu-Servers begann mit der Verwendung einer ISO-Datei und der Auswahl des Betriebssystems Linux.

![VM](Fotos/Start.png)

## Konfiguration des Ubuntu-Servers
Nachdem die VM erstellt wurde, begann die Konfiguration des Servers. Zunächst wurde der Installationstyp ausgewählt: Ubuntu Server für diese Aufgabe.

![](Fotos/Konfigurieren_1.png)

Einigen Schritten wurde ausgelassen, da sie später über die Befehlszeile ausgeführt werden sollten.
![](Fotos/Konfigurieren_2.png)

Bei der Speicherkonfiguration wurden die angegebenen Einstellungen belassen, und es wurde fortgefahren.
![Alt text](Fotos/Konfigurieren_4.png)

Jetzt kam der Schritt zur Benutzer-Einrichtung.
![Alt text](Fotos/Konfigurieren_6.png)

Dann wurde alles heruntergeladen.
![Alt text](Fotos/Konfigurieren_9.png)

Nun konnte ich Befehle verwenden.

Zuerst habe ich 'sudo' verwendet und den Befehl `sudo apt update` eingegeben.
![Alt text](Fotos/Konfigurieren_10.png)

Anschließend wurde `sudo apt install isc-dhcp-server -y` ausgeführt, um DHCP zu installieren.
![Alt text](Fotos/Konfigurieren_11.png)

Danach habe ich die Datei '/etc/default/isc-dhcp-server' bearbeitet, um das Interface auf "ens37" zu ändern. Ich habe die Änderung gespeichert und die Datei geschlossen.
![Alt text](Fotos/Konfigurieren_12.png)

Als nächstes habe ich “/etc/dhcp/dhcpd.conf” bearbeitet. Ich habe diese Datei mit `sudo nano /etc/dhcp/dhcpd.conf` geöffnet.

Bei

- #option domain-name "example.org";
- #option domain-name-servers ns1.example.org, ns2.example.org;

habe ich das # hinzugefügt.

Bei

- authoritative;

habe ich das # entfernt.
![Alt text](Fotos/Konfigurieren_13.png)

Als nächstes habe ich den range definiert. Ich habe diese Angaben eingegeben, die ich aus M117 übernommen habe.
![Alt text](Fotos/Konfigurieren_14.png.png)

Der nächste Schritt war einstellen der IP-Adresse. Zuerst habe ich `cd /etc/netplan/` und dann `sudo nano /etc/netplan/00-installer-config.yaml` eingegeben, um zu dem Fenster zu gelangen.

Jetzt konnte ich bearbeiten. Da ich nur 'ens37' brauchte, habe ich dort 'dhcp4' entfernt und die Adresse 192.168.100.1/24 angegeben.
![Alt text](Fotos/Konfigurieren_15.png)

Danach habe ich `sudo netplan apply` eingegeben, um die Änderungen zu speichern. Zur Kontrolle habe ich `ip addr show ens37` eingegeben und festgestellt, dass die IP-Adresse korrekt war.
![Alt text](Fotos/Konfigurieren_16.png)

Jetzt werde ich den DHCP starten und aktivieren, indem ich diese Befehle eingebe:
- `sudo systemctl start isc-dhcp-server`
- `sudo systemctl enable isc-dhcp-server`

Zuletzt habe ich den Status des DHCP-Servers überprüft mit dem Befehl `sudo systemctl status isc-dhcp-server`. Bei mir funktioniert der DHCP-Server.
![Alt text](Fotos/Konfigurieren_19.png.png)

## Konfiguration des DHCP-Clients
Mein Client ist ein Windows 10 Computer.
Nach dem Einloggen bin ich zu den Einstellungen gegangen. Dort habe ich unter **Netzwerk und Internet** > **Status** das benötigte Netzwerk (Ethernet1 - ens37) ausgewählt.
![Alt text](Fotos/WIN2.png)
![Alt text](Fotos/WIN3.png)
![Alt text](Fotos/WIN4.png)
![Alt text](Fotos/WIN5.png)

Die Option **IP-Adresse automatisch beziehen** wurde ausgewählt.

In der Eingabeaufforderung des Clients habe ich `ipconfig /release` eingegeben, um die IP-Adresse zu sehen. Es zeigt, dass der Computer die IP 192.168.100.2 erhalten hat, die die nächste freie IP nach der des DHCP-Servers ist.
![Alt text](Fotos/WIN7.png)

Zuletzt habe ich noch die Lease Time auf dem DHCP-Server überprüft: `cat /var/lib/dhcp/dhcpd.leases`
![Alt text](Fotos/WIN8.png)