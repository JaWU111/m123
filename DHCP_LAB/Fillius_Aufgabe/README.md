[TOC]
# DHCP Configuration **NEW**
Als Aufgabe habe ich in Filius bekommen einen DHCP Server zu konfigurieren mit einer Stern Topologie mit 3 Computers. 2 dynamisch und 1 Statisch. 
Als erstes habe ich den Server, Switch und die 3 Computers mit Kabel verbunden. Danach erfolgt die Konfiguration des DHCP Servers.
**dhcp**
![Images](./Images/image-1.png)
Der IP Range ist 192.168.0.150 - 192.168.0.170. Ich habe das in den Felder eingefüllt. Diese IP Adressen bekommen die Computer die mit DHCP Adresse dynamisch konfiguriert werden.  Der Letze Schritt hier ist noch den DHCP aktivieren.
Die Statische Adresse Zuweisung erfolgt als nächstes.

![Alt text](image-3.png)
Hier braucht man die MAC-Adresse und die IP. Dies muss man in den Feldern einfüllen und dann hinzufügen.

![Alt text](image-4.png)
Bei den einzelnen Computers muss man noch diese Boxe anhäkeln 😊. 
Mit «DHCP zur Konfiguration verwenden» werden die IP Adressen vom DHCP Server gegeben.
Mit «IP-Weiterleitung aktivieren» werden die IPs an den Compi weitergeleitet.

Als aller letztes habe ich einen Ping gemacht von 192.168.0.150 an 192.168.9.50. Den Ping hat funktioniert.

![Alt text](image-5.png)
![Alt text](image-6.png)
