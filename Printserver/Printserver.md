[TOC]

# Erstellung eines Druckdienstes

In diesem Projekt habe ich auf einem Windows-Client einen Printserver eingerichtet.

## Printserver einrichten

Zuerst öffne ich den Server Manager und füge einen Drucker hinzu.
![Alt-Text](images/1.png)

Im Print Manager trage ich die IP-Adresse meines Druckers ein.
![Alt text](<images/Screenshot 2024-01-22 162413.png>)
![Alt text](<images/Screenshot 2024-01-22 162459.png>)
Anschließend wähle ich den Treiber aus, in diesem Fall "HP Universal Printing PCL 6".
![Alt text](<images/Screenshot 2024-01-22 163034.png>)

Damit ist der Drucker erfolgreich erstellt.
![Alt text](<images/Screenshot 2024-01-22 163133.png>)

Als Test drucke ich eine Testseite.
![Alt text](<images/Screenshot 2024-01-22 155916.png>)

## Auf dem Client drucken

Als erstes habe ich zuerst einen Ping gemacht zum testen ob der client und der Server eine verbindung haben.
![Alt text](images/Ping.png)

Dies hat geklappt. Ich könnte vom Server drucken und von Client mit Nat ebensfalls drucken.