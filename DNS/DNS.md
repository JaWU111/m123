[TOC]
## Bereitstellung des DNS-Dienstes

Unsere Aufgabe bestand darin, einen DNS-Server unter Windows zu konfigurieren und diesen mit einem Windows-Client zu testen. 
![Alt text](<Images/DNS Cover.png>)

### Einrichten des DNS-Servers
Um mit der Aufgabe zu beginnen, war zunächst die Einrichtung einer virtuellen Maschine erforderlich.

#### Erstellung der VM (Virtuellen Maschine)
Ich habe zunächst in VMware Workstation eine neue VM erstellt.
![Server1](Images/Server1.png)

Als Firmware habe ich BIOS ausgewählt.

![Server2](Images/Server_2.png)

Nach der Erstellung der VM habe ich mit der Aktivierung der Serverlizenz begonnen.
![Server3](Images/Server_3.png)

Anschließend habe ich das Betriebssystem heruntergeladen.
![Server4](Images/Server_4.png)
![Server5](Images/Server_5.png)
![Server6](Images/Server_6.png)

Die VM wurde erfolgreich erstellt, und das Betriebssystem wurde heruntergeladen. Der nächste Schritt war das Herunterladen des DNS-Servers.

### Installation des DNS Windows Server 2022
Bevor ich beginne, ändere ich zuerst die IP-Adresse. Ich habe die IP-Adresse meines Computers als DNS-Server eingegeben.
![Konfigurieren DNS 2](Images/Konfigurieren_DNS_2.png)

Dann öffne ich den Server Manager und gehe zu "Manage" > "Add Roles and Features".

![Konfigurieren 3](Images/Konfigurieren_3.png)

Dort wähle ich "Next".
![Konfigurieren 4](Images/Konfigurieren_4.png)

Anschließend wähle ich "Rollenbasierte oder featurebasierte Installation" aus.
![Konfigurieren 5](Images/Konfigurieren_5.png)

Als nächstes wähle ich den entsprechenden Server aus.
![Konfigurieren 6](Images/Konfigurieren_6.png)

Ich wähle den DNS-Server aus und klicke auf "Next".
![Konfigurieren 7](Images/Konfigurieren_7.png)

Unter "Features" klicke ich einfach auf "Next".
![Konfigurieren 8](Images/Konfigurieren_8.png)

Zum Schluss erhalte ich eine Übersicht , was ich installieren möchte.
![Konfigurieren 9](Images/Konfigurieren_9.png)

Der DNS-Server wurde installiert. Nun muss ich den DNS-Server konfigurieren.

### Konfiguration des DNS-Servers
Zuerst drücke ich die Tasten `WIN + R`, um den **dnsmgmt.msc** einzugeben und den DNS-Manager zu öffnen.
![Konfigurieren 10](Images/Konfigurieren_10.png)

#### Forward-Lookup-Zone
*Was ist eine Forward Lookup Zone?*
Domainnamen wie 20min.ch werden aufgelöst zu ihre entsprechende IP-Adresse. 

Um eine Zone hinzuzufügen, klicke ich mit der rechten Maustaste auf "Forward Lookup Zone" und wähle "New Zone".
![Konfigurieren 11](Images/Konfigurieren_11.png)
![Konfigurieren 12](Images/Konfigurieren_12.png)

Ich wähle "Primäre Zone" aus.
![Konfigurieren 13](Images/Konfigurieren_13.png)

Als Zone Name gebe ich **Sotagmbh.ch** ein.
![Konfigurieren 14](Images/Konfigurieren_14.png)

Für die Zonendatei gebe ich **Sotagmbh.ch.dns** ein.
![Konfigurieren 15](Images/Konfigurieren_15.png)

Hier wähle ich "Keine dynamischen Updates zulassen".
![Konfigurieren 16](Images/Konfigurieren_16.png)

Damit ist die erste Zone fertig.
![Konfigurieren 17](Images/Konfigurieren_17.png)
![Konfigurieren 18](Images/Konfigurieren_18.png)

#### Reverse Lookup Zone
*Was ist eine Reverse Lookup Zone?*
Die Reverse Lookup Zone ist das Gegenteil der Forward Lookup Zone. Sie setzt IP-Adressen in Domainnamen um.

Um eine Zone hinzuzufügen, klicke ich mit der rechten Maustaste auf "Reverse Lookup Zone" und wähle "New Zone".
![Konfigurieren 19](Images/Konfigurieren_19.png)

Ich wähle Primary Zone aus.
![Konfigurieren 20](Images/Konfigurieren_20.png)

Dann wähle ich IPv4 aus.
![Konfigurieren 21](Images/Konfigurieren_21.png)

Als nächstes gebe ich die ersten drei Oktetten meiner IP-Adresse vom DNS-Server an.
![Konfigurieren 22](Images/Konfigurieren_22.png)

Den Dateinamen, den ich erhalten habe, behalte ich und klicke auf "Next".
![Konfigurieren 23](Images/Konfigurieren_23.png)

Damit ist die neue Zone erstellt.
![Konfigurieren 24](Images/Konfigurieren_24.png)

#### Zone Lookup File
Jetzt erstelle ich die A-Records.

Zuerst klicke ich mit der rechten Maustaste auf meine neue Forward-Zonendatei und dann auf "NEW HOST".

Danach gebe ich meinem Host einen Namen und eine IP-Adresse.
"Create associated pointer record" habe ich ebenfalls ausgewählt.

![Konfigurieren 25](Images/Konfigurieren_25.png)

![Konfigurieren 26](Images/Konfigurieren_26.png)

Dann habe ich dasselbe für den Client gemacht.

![Konfigurieren 27](Images/Konfigurieren_27.png)

Beide sind jetzt fertig.
![Konfigurieren 28](Images/Konfigurieren_28.png)

#### Forwarder einfügen
Ich gehe auf dem DNS-Server > Rechtsklick auf Properties > Forwarder und dann auf "Edit".
![Konfigurieren 29](Images/Konfigurieren_29.png)

Ich habe mich für den Google DNS-Server entschieden.

![Konfigurieren 30](Images/Konfigurieren_30.png)

Der DNS-Server ist nun vollständig konfiguriert.

## DNS-Client erstellen

Für den Client habe ich dieselbe VM wie für den DNS-Server verwendet. Jedoch habe ich einige Einstellungen gemacht:

Den DHCP-Dienst ausschalten, manuell eine IP-Adresse zuweisen und den DNS-Server eingeben.
![Client 1](Images/Client_1.png)

Dann habe ich getestet, ob der Client den DNS-Server pingen kann. Dies war erfolgreich.
![Client 2](Images/Client_2.png)

Der Client ist ebenfalls fertig.

## Testprotokoll
Zuletzt werde ich mit dem Client testen, ob der DNS-Server funktioniert.

Zuerst habe ich CMD geöffnet.
Mit dem Befehl `nslookup...` kann man testen, ob der DNS funktioniert.


Zuerst habe ich meine 2 Hosts ausprobiert.

![Test 1](Images/Test_1.png)

Dann habe ich den Reverse-Test durchgeführt, der ebenfalls funktioniert.
![Test 2](Images/Test_2.png)

Den Forwarder habe ich ebenfalls getestet. Ich habe versucht, *20min.ch* zu pingen.
![Test 3](Images/Test_3.png)
![Test 4](Images/Test_4.png)

Der DNS-Server funktioniert wie er sollte.

![Alt text](<Images/emoji reaction pic.jpeg>)