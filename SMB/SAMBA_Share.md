# Linux-Deployment für „SAMBA Share“

In dieser Aufgabe musste ich einen Samba-Dienst auf einem Ubuntu-Server  installieren.

Zur Vorbereitung habe ich überprüft ob mein Windows-Client  eine Verbindung zum Ubuntu-Server herstellen kann. Ich habe einen Ping-Test grmacht.

![Screenshot 2024-01-21 131704](<Images/Screenshot 2024-01-21 131704.png>)


## SMB-Installation

Zuerst führe ich den Befehl `sudo apt-get install` aus.

![Screenshot 2024-01-21 113804](<Images/Screenshot 2024-01-21 113804.png>)

Danach aktualisiere ich die Paketliste und installiere Samba sowie Net-Tools mit den Befehlen:

```bash
sudo apt-get update && sudo apt-get install samba net-tools -y
```

![Screenshot 2024-01-21 113931](<Images/Screenshot 2024-01-21 113931.png>)

Anschließend passe ich die Konfigurationsdatei an.

![Screenshot 2024-01-21 114055](<Images/Screenshot 2024-01-21 114055.png>)
![Screenshot 2024-01-21 122613](<Images/Screenshot 2024-01-21 122613.png>)

Nach dem Speichern füge ich einen SambaShare-Benutzer hinzu.

![Screenshot 2024-01-21 114715](<Images/Screenshot 2024-01-21 114715.png>)

Zuletzt führe ich einen Neustart durch.

![Screenshot 2024-01-21 114757](<Images/Screenshot 2024-01-21 114757.png>)

## Windows-Zugriff auf den freigegebenen Ordner

Jetzt teste ich, ob der SMB-Dienst ordnungsgemäß funktioniert. Dazu öffne ich den Explorer, klicke mit der rechten Maustaste auf "This PC" und wähle "Add Network Location".

![Screenshot 2024-01-21 124242](<Images/Screenshot 2024-01-21 124242.png>)

Dann gebe ich bei "Folder" die Adresse ein: \\\192.168.0.30\Pictures (\\\IP\Shared Folder).

![Screenshot 2024-01-21 124223](<Images/Screenshot 2024-01-21 124223.png>)

Ich habe erfolgreich auf den freigegebenen Ordner zugegriffen.

