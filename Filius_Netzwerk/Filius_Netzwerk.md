[TOC]

# Filius Netzwerk Aufgabe

In dieser Aufgabe war es erforderlich, die Konfiguration eines Firmennetzwerks vorzunehmen.

## Netzwerk erstellen

Zu Beginn habe ich die Namen, IP-Adressen, Gateway und Domain Name Server festgelegt.

Für drei Computer habe ich die IP-Adressen manuell eingetragen, während bei den anderen die DHCP-Funktion aktiviert war.

![Netzwerkkonfiguration](<Images/Screenshot 2024-01-29 133123.png>)

![Netzwerkkonfiguration](<Images/Screenshot 2024-01-29 133132.png>)

![Netzwerkkonfiguration](<Images/Screenshot 2024-01-29 133137.png>)

![Netzwerkkonfiguration](<Images/Screenshot 2024-01-29 133417.png>)

Anschließend habe ich alle Geräte miteinander verbunden.

![Netzwerkverbindung](<Images/Screenshot 2024-01-29 133149.png>)

Danach habe ich die Switches am Router konfiguriert.

![Switch-Konfiguration](<Images/Screenshot 2024-01-29 133158.png>)

Im nächsten Schritt habe ich den DNS-Server und den Web-Server eingerichtet.

![DNS-Konfiguration](<Images/Screenshot 2024-01-29 133238.png>)

Die A-Records wurden dann im DNS eingetragen.

![A-Record-Konfiguration](<Images/Screenshot 2024-01-29 133252.png>)

Abschließend habe ich den DNS-Server mit einem Ping getestet.

![DNS-Test](<Images/Screenshot 2024-01-29 133405.png>)